pragma solidity ^0.4.24;

contract Gaming {
    /* My online gaming contract */
    address public owner;
    bool public online;

    struct Player {
        uint wins;
        uint losses;
    }

    mapping (address => Player) public players;

    constructor() public payable {
        owner = msg.sender;
        online = true;
    }

    /* function modifier */
    modifier isOwner() {
        require(msg.sender == owner, "Only owner can call this function");
        _;
    }

    /* events */
    event GameFunded(address funder, uint amount);

    event PlayerWon(address player, uint amount, uint mysteryNumber, uint displayedNumber);
    event PlayerLost(address player, uint amount, uint mysteryNumber, uint displayedNumber);

    function mysteryNumber() internal view returns (uint) {
        uint randomNumber = uint(blockhash(block.number-1))%10 + 1;
        return randomNumber;
    }

    function determineWinner(uint number, uint display, bool guess) public pure returns (bool) {
        if (guess == true) {
            if (number > display) {
                return true;
            }
            if (number < display) {
                return false;
            }
        } else if (guess == false) {
            if (number > display) {
                return false;
            }
            if (number < display) {
                return true;
            }
        }
    }

    function winOrLose(uint display, bool guess) external payable returns (bool, uint) {
        /* Use true for a higher guess, false for a lower guess */
        require(online == true);
        require(msg.sender.balance > msg.value, "Insufficient funds");
        uint mysteryNumber_ = mysteryNumber();
        bool isWinner = determineWinner(mysteryNumber_, display, guess);
        Player storage player = players[msg.sender];
        if (isWinner == true) {
            /* Player won */
            player.wins += 1;
            msg.sender.transfer(msg.value * 2); // return the amount wagered plus the ether sent with the transaction
            emit PlayerWon(msg.sender, msg.value, mysteryNumber_, display);
            return (true, mysteryNumber_);
        } else if (isWinner == false) {
            /* Player lost */
            player.losses += 1;
            emit PlayerLost(msg.sender, msg.value, mysteryNumber_, display);
            return (false, mysteryNumber_);
        }
    }

    function withdrawFunds() public isOwner {
        msg.sender.transfer(address(this).balance);
    }

    function fundGame() public isOwner payable {
        emit GameFunded(msg.sender, msg.value);
    }

    /* fallback function to handle unexpected messages */
    function() public payable {
    }
}
