# Sample Solidity application

## Running geth server
```
geth --testnet --syncmode "light" --rpc --rpcapi db,eth,net,web3,personal,admin --cache=1024 --rpcport 8545
```

## Connecting to geth server
```
geth attach http://127.0.0.1:8545
```
